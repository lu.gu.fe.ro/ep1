#ifndef CANDIDATO_HPP
#define CANDIDATO_HPP

#include <string>
#include <vector>

class Produto
{
    private:
        string codigo_produto;
        string nome_produto;
        vector<string> categorias_produto;
        double preco_produto;
        int quantidade_em_estoque_produto;

    public:
        Produto();
        ~Produto();
        void set_categorias_produto(vector<string> categorias_produto);
        void set_codigo_produto(string codigo_produto);
        void set_preco_produto(double preco_produto);
        void set_quantidade_em_estoque_produto(int quantidade_em_estoque_produto);
        void set_nome_produto(string nome_produto);

        vector<string> get_categorias_produto();
        string get_codigo_produto();
        double get_preco_produto();
        int get_quantidade_em_estoque_produto();
        string get_nome_produto();

};

#endif
